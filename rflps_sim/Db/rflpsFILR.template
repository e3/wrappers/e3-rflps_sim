record(bi, "$(PREFIX)$(SGNL):PsuON-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ON-RB")
    alias("$(PREFIX)$(SGNL):ON-RB")

    field(DESC, "Filament PSU Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=0" )
    field(ZNAM, "PSU-OFF")
    field(ONAM, "PSU-ON")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):I-SPHIHI-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ISPHIHI-RB")
    alias("$(PREFIX)$(SGNL):ISPHIHI-RB")

    field(DESC, "Filament Current HIHI Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=1" )
    field(ZNAM, "I-SP - I-HIHI OK")
    field(ONAM, "I-SP >= I-HIHI WARN")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):SetPointReached-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-SPREACHED-RB")
    alias("$(PREFIX)$(SGNL):SPREACHED-RB")

    field(DESC, "Filament Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=2" )
    field(ZNAM, "INCOMPLETED")
    field(ONAM, "COMPLETED")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):IREG-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IREG-RB")

    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=3" )

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):LocalRemote-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-LOCREM-RB")
    alias("$(PREFIX)$(SGNL):LOCREM-RB")

    field(DESC, "Filament PSU Communication Mode")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=4" )
    field(ZNAM, "LOCAL")
    field(ONAM, "REMOTE")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):NotLatch-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-NLTCH-RB")
    alias("$(PREFIX)$(SGNL):NLTCH-RB")

    field(DESC, "Filament Latch Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=5" )
    field(ZNAM, "N-LTCH")
    field(ONAM, "LTCHD")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):NotForced-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-NFRC-RB")
    alias("$(PREFIX)$(SGNL):NFRC-RB")

    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=6" )
    field(ZNAM, "C-FRC")
    field(ONAM, "N-FRC")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):Warn-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WRN-RB")
    alias("$(PREFIX)$(SGNL):WRN-RB")

    field(DESC, "Filament PSU Warning Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+0 T=INT8 B=7" )
    field(ZNAM, "WRN-OK")
    field(ONAM, "WRN")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):Ilck-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-INT-RB")
    alias("$(PREFIX)$(SGNL):INT-RB")

    field(DESC, "Filament PSU Interlock Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=0" )
    field(ZNAM, "ITLCK-OK")
    field(ONAM, "ITLCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):Force-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-FRC-RB")
    alias("$(PREFIX)$(SGNL):FRC-RB")

    field(DESC, "Filament PSU Forced Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=1" )
    field(FLNK, "$(PREFIX)$(SGNL):Force-INT")
    field(ZNAM, "N-FRC")
    field(ONAM, "FRCD")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):FaultStatus-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-FAULTSTAT-RB")
    alias("$(PREFIX)$(SGNL):FAULTSTAT-RB")

    field(DESC, "Filament PSU Fault Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=2" )
    field(ZNAM, "FAULT-STAT OK")
    field(ONAM, "FAULT-STAT ITLCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):IRampON-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IRAMPON-RB")
    alias("$(PREFIX)$(SGNL):IRAMPON-RB")

    field(DESC, "Filament PSU Current Ramp mode")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=3" )
    field(FLNK, "$(PREFIX)$(SGNL):IRampON-INT")
    field(ZNAM, "STEP-ON")
    field(ONAM, "RAMP-ON")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):VRampON-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-VRAMPON-RB")
    alias("$(PREFIX)$(SGNL):VRAMPON-RB")

    field(DESC, "Filament PSU Voltage Ramp mode")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=4" )
    field(FLNK, "$(PREFIX)$(SGNL):VRampON-INT")
    field(ZNAM, "V-RAMP-OFF")
    field(ONAM, "V-RAMP-ON")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):Calib-ON-RB") {
    alias("$(PREFIX)$(SGNL):CAL-ON-RB")

    field(DESC, "Filament Calibration Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=5" )
    field(FLNK, "$(PREFIX)$(SGNL):Calib-ON-INT")
    field(ZNAM, "CAL-OFF")
    field(ONAM, "CAL-ON")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):BlackHeat-Cmd-RB") {
    field(DESC, "Filament Black Heat Command")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+1 T=INT8 B=6" )
    field(FLNK, "$(PREFIX)$(SGNL):BlackHeat-Cmd-INT")
    field(ZNAM, "BlackHeat ON")
    field(ONAM, "BlackHeat OFF")

    info(ARCHIVE_THIS, "")
}

record(mbbi, "$(PREFIX)$(SGNL):OpMode-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-OPMODE-RB")
    alias("$(PREFIX)$(SGNL):OPMODE-RB")

    field(DESC, "Filament Operation mode")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP,  "@$(PLC)/$(SOFF)+2 T=INT16" )
    field(ZRVL, "0")
    field(ONVL, "1")
    field(TWVL, "2")
    field(ZRST, "MANUAL")
    field(ONST, "30 min.")
    field(TWST, "60 min.")
    field(FLNK, "$(PREFIX)$(SGNL):OpMode-INT")
    field(NOBT, "3")
    field(SHFT, "0")

    info(ARCHIVE_THIS, "")
}

record(mbbi, "$(PREFIX)$(SGNL):CurrentState-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-CURRENTSTAT-RB")
    alias("$(PREFIX)$(SGNL):CURRENTSTAT-RB")

    field(DESC, "Filament Current Status mode")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+4 T=INT16" )
    field(ZRVL, "0")
    field(ONVL, "1")
    field(TWVL, "2")
    field(THVL, "3")
    field(FRVL, "4")
    field(FVVL, "5")
    field(SXVL, "6")
    field(SVVL, "7")
    field(EIVL, "8")
    field(NIVL, "9")
    field(TEVL, "10")
    field(ZRST, "STOPPED")
    field(ONST, "INIT")
    field(TWST, "RAMPING")
    field(THST, "HEATING")
    field(FRST, "READY")
    field(FVST, "BLACK-HEAT")
    field(SXST, "NOT_DEFINED")
    field(SVST, "NOT_DEFINED")
    field(EIST, "NOT_DEFINED")
    field(NIST, "NOT_DEFINED")
    field(TEST, "ERROR")
    field(NOBT, "11")
    field(SHFT, "0")

    info(ARCHIVE_THIS, "")
}

record(longin, "$(PREFIX)$(SGNL):Error-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERR-RB")
    alias("$(PREFIX)$(SGNL):ERR-RB")

    field(DESC, "Filament Error code")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16" )

    info(ARCHIVE_THIS, "")
}

#Bit0:  SP3 Current value can't be zero if Ramped!
#Bit1:  PSU SetPoint values are not in incresing order!
#Bit2:  Interlock PSU has internal faliure!
#Bit3:  Interlock during Warmup!

#Bit4:  Interlock absulute maximum exeeded!
#Bit5:  Interlock High limit exeeded while PSU_ON!
#Bit6:  Interlock Low limit exeeded while PSU_ON!
#Bit7:  PSU can't be turned on because of an error!

#Bit8:  Initialisation error. No DTYP is given!
#Bit9:  PSU is in LOCAL mode. Set to REMOTE!
#Bit10: Analog Input IMON SCALE error!
#Bit11: Analog Input WMON SCALE error!

#Bit12: Analog Input VMON SCALE error!
#Bit13: Analog OutPut IPGM/VPGM SCALE error!
#Bit14: SP1 Current value can't be zero if Ramped!
#Bit15: SP2 Current value can't be zero if Ramped!

record(bi, "$(PREFIX)$(SGNL):ErrISpHIHI-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRISPIHIHI-RB")
    alias("$(PREFIX)$(SGNL):ERRISPIHIHI-RB")

    field(DESC, "Filament SP Not zero Fault")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=0" )
    field(ZNAM, "SP3 OK")
    field(ONAM, "SP3 MUST BE VAL > 0")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrDisturbanceTol-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRDISTOL-RB")
    alias("$(PREFIX)$(SGNL):ERRDISTOL-RB")

    field(DESC, "Filament Disturbance Tolerance Fault")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=1" )
    field(ZNAM, "DISTURBANCE TOL OK")
    field(ONAM, "DISTURBANCE TOL ERR")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrMaxTime-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRTIME-RB")
    alias("$(PREFIX)$(SGNL):ERRTIME-RB")

    field(DESC, "Filament Max time fault")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=2" )
    field(ZNAM, "TIME OK")
    field(ONAM, "MAX TIME < 2H40M")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrOnState-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRONSTAT-RB")
    alias("$(PREFIX)$(SGNL):ERRONSTAT-RB")

    field(DESC, "Filament Stat fault")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=3" )
    field(ZNAM, "OK")
    field(ONAM, "MISMATCH ON ST-CMD")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrHIHI1-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRHIHI1-RB")
    alias("$(PREFIX)$(SGNL):ERRHIHI1-RB")

    field(DESC, "Filament HIHI Interlock Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=4" )
    field(ZNAM, "HIHI OK")
    field(ONAM, "HIHI ERROR")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrHIGH1-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRHIGH1-RB")
    alias("$(PREFIX)$(SGNL):ERRHIGH1-RB")

    field(DESC, "Filament HIGH Interlock Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=5" )
    field(ZNAM, "HIGH OK")
    field(ONAM, "HIGH ITLCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrLow1-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRLOW1-RB")
    alias("$(PREFIX)$(SGNL):ERRLOW1-RB")

    field(DESC, "Filament LOW Interlock Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=6" )
    field(ZNAM, "LOW OK")
    field(ONAM, "LOW ITLCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrPsu-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRPSU-RB")
    alias("$(PREFIX)$(SGNL):ERRPSU-RB")

    field(DESC, "Filament PSU Error")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=7" )
    field(ZNAM, "PSU OK")
    field(ONAM, "PSU ERROR")
    field(ZSV, "NO_ALARM")
    field(OSV, "MINOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrLocRem-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRLOCREM-RB")
    alias("$(PREFIX)$(SGNL):ERRLOCREM-RB")

    field(DESC, "Filament Comm Mode Fault")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=8" )
    field(ZNAM, "PSU REMOTE OK")
    field(ONAM, "PSU MUST BE IN REM")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrISetPoint-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRISP-RB")
    alias("$(PREFIX)$(SGNL):ERRISP-RB")

    field(DESC, "Filament Current SP Fault")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=9" )
    field(ZNAM, "I-SP OK")
    field(ONAM, "I-SP MUST BE > 0")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrHIHI-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRHIHI-RB")
    alias("$(PREFIX)$(SGNL):ERRHIHI-RB")

    field(DESC, "Filament HIHI Interlock Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=10" )
    field(ZNAM, "HIHI OK")
    field(ONAM, "HIHI INTERLOCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrLOLO-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRLOLO-RB")
    alias("$(PREFIX)$(SGNL):ERRLOLO-RB")

    field(DESC, "Filament LOLO Interlock Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=11" )
    field(ZNAM, "LOLO OK")
    field(ONAM, "LOLO INTERLOCK")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrIMonitorScale-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRIMONSCALE-RB")
    alias("$(PREFIX)$(SGNL):ERRIMONSCALE-RB")

    field(DESC, "Filament Current Scale Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=12" )
    field(ZNAM, "I-SCALE OK")
    field(ONAM, "I-SCALE ERROR")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrWMonitorScale-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRWMONSCALE-RB")
    alias("$(PREFIX)$(SGNL):ERRWMONSCALE-RB")

    field(DESC, "Filament Power Scale Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=13" )
    field(ZNAM, "W-SCALE OK")
    field(ONAM, "W-SCALE ERROR")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrIPGMScale-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRIPGMSCALE-RB")
    alias("$(PREFIX)$(SGNL):ERRIPGMSCALE-RB")

    field(DESC, "Filament IPGM Scale Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=14" )
    field(ZNAM, "OPGM SCALE OK")
    field(ONAM, "IPGM SCALE ERROR")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

record(bi, "$(PREFIX)$(SGNL):ErrFault-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ERRFAULT-RB")
    alias("$(PREFIX)$(SGNL):ERRFAULT-RB")

    field(DESC, "Filament Fault Status")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+6 T=INT16 B=15" )
    field(ZNAM, "FIL FAULT_STAT OK")
    field(ONAM, "FIL FAULT_STAT")
    field(ZSV, "NO_ALARM")
    field(OSV, "MAJOR")

    info(ARCHIVE_THIS, "")
}

# End errors list

record(ai, "$(PREFIX)$(SGNL):WarmTimeLeft-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TWARMTLEFT-RB")
    alias("$(PREFIX)$(SGNL):TWARMTLEFT-RB")

    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+8 T=INT32" )
    field(MDEL, "-1")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):RampTime-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TRAMP-RB")
    alias("$(PREFIX)$(SGNL):TRAMP-RB")

    field(DESC, "Filament Ramp Time RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+20 T=INT16" )
    field(EGU, "min")
    field(FLNK, "$(PREFIX)$(SGNL):RampTime-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):HeatingTime-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-THEATING-RB")
    alias("$(PREFIX)$(SGNL):THEATING-RB")

    field(DESC, "Filament Heating setpoint RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+22 T=INT16" )
    field(EGU, "min")
    field(FLNK, "$(PREFIX)$(SGNL):HeatingTime-INT")

    info(ARCHIVE_THIS, "")
}

# I - set-points
record(ai, "$(PREFIX)$(SGNL):I-SP-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ISETPOINT-RB")
    alias("$(PREFIX)$(SGNL):ISETPOINT-RB")

    field(DESC, "Filament Current Setpoint RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+44 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "A")
    field(FLNK, "$(PREFIX)$(SGNL):I-SP-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):VMon-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-VMON-RB")
    alias("$(PREFIX)$(SGNL):VMON-RB")

    field(DESC, "Filament Voltage Monitor")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+32 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "V")
    field(PREC, "$(PREC)")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):IMon-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IMON-RB")
    alias("$(PREFIX)$(SGNL):IMON-RB")

    field(DESC, "Filament Current Monitor")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+36 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "A")
    field(PREC, "$(PREC)")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):WMon-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WMON-RB")
    alias("$(PREFIX)$(SGNL):WMON-RB")

    field(DESC, "Filament Power Monitor")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+40 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "W")
    field(PREC, "$(PREC)")

    info(ARCHIVE_THIS, "")
}

# I - threshold interlock levels
record(ai, "$(PREFIX)$(SGNL):IHIHI-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IHIHI-RB")

    field(DESC, "Filament Current HIHI Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+48 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "A")
    field(FLNK, "$(PREFIX)$(SGNL):IHIHI-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):ILOLO-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ILOLO-RB")

    field(DESC, "Filament Current LOLO Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+52 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "A")
    field(FLNK, "$(PREFIX)$(SGNL):ILOLO-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):IHIGH-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IHIGH-RB")

    field(DESC, "Filament Current HIGH Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+56 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "A")
    field(FLNK, "$(PREFIX)$(SGNL):IHIGH-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):ILOW-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-ILOW-RB")

    field(DESC, "Filament Current LOW Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+60 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "A")
    field(FLNK, "$(PREFIX)$(SGNL):ILOW-INT")

    info(ARCHIVE_THIS, "")
}

# W - threshold interlock levels
record(ai, "$(PREFIX)$(SGNL):WHIHI-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WHIHI-RB")

    field(DESC, "Filament Power HIHI Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+64 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "W")
    field(FLNK, "$(PREFIX)$(SGNL):WHIHI-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):WLOLO-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WLOLO-RB")

    field(DESC, "Filament Power LOLO Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+68 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "W")
    field(FLNK, "$(PREFIX)$(SGNL):WLOLO-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):WHIGH-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WHIGH-RB")

    field(DESC, "Filament Power HIGH Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+72 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "W")
    field(FLNK, "$(PREFIX)$(SGNL):WHIGH-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):WLOW-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WLOW-RB")

    field(DESC, "Filament Power LOW Alarm RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+76 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "W")
    field(FLNK, "$(PREFIX)$(SGNL):WLOW-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):IPGM-SP-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IEGUSP-RB")
    alias("$(PREFIX)$(SGNL):IEGUSP-RB")

    field(DESC, "Filament Current Output Monitor")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+80 T=REAL32" )
    field(PREC, "$(PREC)")
    field(EGU, "A")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):IMax-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-IMAX-RB")
    alias("$(PREFIX)$(SGNL):IMAX-RB")

    field(DESC, "Filament Maximum Current RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+84 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "A")
    field(PREC, "$(PREC)")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):WMax-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-WMAX-RB")
    alias("$(PREFIX)$(SGNL):WMAX-RB")

    field(DESC, "Filament Maximum Power RB")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+88 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "W")
    field(PREC, "$(PREC)")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):TolInWarmUp-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TOLInWARMUP-RB")
    alias("$(PREFIX)$(SGNL):TOLInWARMUP-RB")

    field(DESC, "Filament Current tolerance in warmup")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+92 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "A")
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL):TolInWarmUp-INT")

    info(ARCHIVE_THIS, "")
}

record(waveform, "$(PREFIX)$(SGNL):Timestamp-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TMSTMP-RB")
    alias("$(PREFIX)$(SGNL):TMSTMP-RB")

    field(DESC, "Timestamp waveform")
    field(SCAN, "I/O Intr")
    field(DTYP, "S7plc")
    field(INP, "@$(PLC)/$(SOFF)+96 T=TIME")
    field(FTVL, "CHAR")
    field(NELM, "8")
    field(FLNK, "$(PREFIX)$(SGNL):Timestamp-Conv")
}

record(aSub, "$(PREFIX)$(SGNL):Timestamp-Conv") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TMSTMP-Conv")

    field(DESC, "Filament Time Conv")
    field(SNAM, "convert_timestamp")
    field(INPA, "$(PREFIX)$(SGNL):Timestamp-RB NPP")
    field(FTA, "CHAR")
    field(FTVA, "STRING")
    field(NOA, "8")
    field(OUTA, "$(PREFIX)$(SGNL):Timestamp PP")
    field(BRSV, "INVALID")
}

record(stringin, "$(PREFIX)$(SGNL):Timestamp") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TMSTMP")

    field(DESC, "Filament Timestamp")
}

record(ai, "$(PREFIX)$(SGNL):TotalTime-RB") {
    alias("$(PREFIX)$(OLD_SGNL=$(SGNL))-TOTALTTIME-RB")
    alias("$(PREFIX)$(SGNL):TOTALTTIME-RB")

    field(DESC, "Filament Total Warm time")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+104 T=INT32" )
    field(EGU, "min")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):FilUpTime-Baseline-RB") {
    alias("$(PREFIX)$(SGNL):FILUPTIME-BASELINE-RB")

    field(DESC, "Filament Uptime Baseline")
    field(SCAN, "I/O Intr")
    field(DTYP, "S7plc")
    field(INP, "@$(PLC)/$(SOFF)+108 T=UINT32")
    field(FLNK, "$(PREFIX)$(SGNL):FilUpTime-Baseline-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):FilUpTime-RB") {
    alias("$(PREFIX)$(SGNL):FILUPTIME-RB")

    field(DESC, "Filament Uptime")
    field(SCAN, "I/O Intr")
    field(DTYP, "S7plc")
    field(INP, "@$(PLC)/$(SOFF)+112 T=UINT32")
    field(EGU, "seconds")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):KLY-Serial-RB") {
    field(DESC, "Klystron Serial")
    field(SCAN, "I/O Intr")
    field(DTYP, "S7plc")
    field(INP, "@$(PLC)/$(SOFF)+116 T=UINT32")
    field(FLNK, "$(PREFIX)$(SGNL):KLY-Serial-INT")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(PREFIX)$(SGNL):BlackHeat-I-RB") {
    field(DESC, "Filament Current Power Saving Set-point")
    field(DTYP, "S7plc")
    field(SCAN, "I/O Intr")
    field(INP, "@$(PLC)/$(SOFF)+120 T=REAL32" )
    field(MDEL, "-1")
    field(EGU, "A")
    field(PREC, "$(PREC)")
    field(FLNK, "$(PREFIX)$(SGNL):BlackHeat-I-INT")

    info(ARCHIVE_THIS, "")
}
